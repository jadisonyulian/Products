﻿

namespace Products.Domain
{
  using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        [Required(ErrorMessage ="The fiel {0} is requerid.")]
        [MaxLength(50,ErrorMessage ="The field {0} only can cantain {1} charaters length")]
        [Index("Category_Description_Index", IsUnique =true)]
        public string Description { get; set; }
    }
}
